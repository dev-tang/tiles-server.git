use serde::Deserialize;

#[derive(Deserialize)]
pub struct Info {
    pub tiles: String,
    pub z: i32,
    pub x: i32,
    pub y: i32,
}